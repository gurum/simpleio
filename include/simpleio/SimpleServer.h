/*
 * SimpleServer.h
 *
 *  Created on: Apr 13, 2016
 *      Author: buttonfly
 */

#ifndef GURUM_SIMPLESERVER_H_
#define GURUM_SIMPLESERVER_H_

#ifndef GCC_VERSION
#define GCC_VERSION (__GNUC__ * 10000 \
				 + __GNUC_MINOR__ * 100 \
                     + __GNUC_PATCHLEVEL__)
#endif

#define USE_CC_THREAD 1

#if GCC_VERSION > 40800
#define USE_CC_THREAD	1
#endif

#include <string>
#if USE_CC_THREAD
#include <thread>
#include <mutex>
#else
#include <pthread.h>
#endif

#include <sys/un.h>
#include <sys/socket.h>
#include <stdint.h>
#include <netinet/in.h>



#ifndef OVERRIDE
#define OVERRIDE
#endif

namespace gurum {

class SimpleServerDelegate {
public:
	virtual void connected(int sck){}
	virtual void disconnected(int sck){}
	virtual void read(int sck, uint8_t *buf, int len){}
};

class SimpleServer {
public:
	SimpleServer();
	virtual ~SimpleServer();

	void setDelegate(SimpleServerDelegate *delegate) {
		_delegate = delegate;
	}
	virtual int start();
	virtual void stop();
	bool isStarted() {
		return _started;
	}
	bool isStopped() {
		return _stopped;
	}

	void setTimeout(uint64_t ms) {
		_timeout = ms;
	}

	virtual int send(uint8_t *buf, size_t len) {return 0;}

//	virtual int client() { return 0; }

protected:
	virtual int open()=0;
	virtual struct sockaddr *sockaddr()=0;
	virtual size_t sockaddr_len()=0;
	void lock();
	void unlock();

protected:
	virtual void run(int clnt);

#if USE_CC_THREAD
#else
	static void thr(void *arg);
#endif

protected:
	SimpleServerDelegate *_delegate;
	bool _stopped;
	bool _started;
#if USE_CC_THREAD
	std::thread _thread;
	std::recursive_mutex _lck;
#else
	pthread_t _thread;
	pthread_mutex_t _lck;
#endif
	int _sck;

	fd_set _readfds;
	int _fd_max;
	uint64_t _timeout;
};

class SimpleUnixServer : public SimpleServer {
public:
	SimpleUnixServer();
	virtual ~SimpleUnixServer();

	void setUnixSockPath(const std::string unixSockPath) {
		_unixSockPath = unixSockPath;
	}

	void stop() OVERRIDE;

protected:
	int open() OVERRIDE;
	struct sockaddr *sockaddr() OVERRIDE;
	size_t sockaddr_len() OVERRIDE;

private:
	struct sockaddr_un  _serveraddr;
	struct sockaddr_un  _clntaddr;
	std::string _unixSockPath;
};

class SimpleTcpServer : public SimpleServer {
public:
	SimpleTcpServer();
	virtual ~SimpleTcpServer();

	void setPort(uint16_t port) {
		_port = port;
	}

protected:
	virtual int open() OVERRIDE;
	struct sockaddr *sockaddr() OVERRIDE;
	size_t sockaddr_len() OVERRIDE;

protected:
	struct sockaddr_in  _serveraddr;
	struct sockaddr_in _clntaddr;
	uint16_t _port;
};

class SimpleUdpServer : public SimpleTcpServer {
public:
	SimpleUdpServer();
	virtual ~SimpleUdpServer();

protected:
	virtual int open() OVERRIDE;
};

class SimplePipeServer : public SimpleServer {
public:
	SimplePipeServer();
	virtual ~SimplePipeServer();

	void setPipePath(const std::string pipePath) {
		_path = pipePath;
	}

	virtual int send(uint8_t *buf, size_t len) OVERRIDE;

	void stop() OVERRIDE;
	//@deprecated. It should not be used.
	virtual int client() OVERRIDE { return _fd; }

protected:
	virtual void run(int clnt) OVERRIDE;
	int open() OVERRIDE;
	struct sockaddr *sockaddr() { return nullptr; };
	size_t sockaddr_len() { return 0;};

private:
	std::string _path;
	int _fd=1; //default: stdout
	int _id=-1;
	int _wd=-1;
};

} /* namespace gurum */

#endif /* GURUM_SIMPLESERVER_H_ */
