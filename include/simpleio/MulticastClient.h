/*
 * MulticastClient.h
 *
 *  Created on: May 1, 2016
 *      Author: buttonfly
 */

#ifndef MULTICASTCLIENT_H_
#define MULTICASTCLIENT_H_

#include "simpleio/SimpleClient.h"
#include <stdint.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#ifndef OVERRIDE
#define OVERRIDE
#endif

#define UDP_BUFFER_SIZE	1316
#define RTP_BUFFER_SIZE 1328

namespace gurum {

class MulticastClient : public SimpleClient{
public:
	MulticastClient();
	virtual ~MulticastClient();

	void setIP(std::string ip) {
		_ip = ip;
	}
	void setPort(uint16_t port) {
		_port = port;
	}

protected:
	virtual int open() OVERRIDE;
	virtual int recv(int fd, uint8_t *buf, size_t len) OVERRIDE;
	virtual void run() OVERRIDE;

private:
	std::string _ip;
	uint16_t _port;
	struct sockaddr_in _serveraddr;
	struct ip_mreq _mreq;
};

} /* namespace gurum */

#endif /* MULTICASTCLIENT_H_ */
