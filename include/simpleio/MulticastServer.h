/*
 * MulticastServer.h
 *
 *  Created on: May 1, 2016
 *      Author: buttonfly
 */

#ifndef MULTICASTSERVER_H_
#define MULTICASTSERVER_H_

#include <stdio.h>
#include <stdint.h>
#include <string>
#include <sys/socket.h>
#include <stdint.h>
#include <netinet/in.h>
#include "simpleio/SimpleServer.h"

#ifndef OVERRIDE
#define OVERRIDE
#endif

#define UDP_BUFFER_SIZE	1316
#define RTP_BUFFER_SIZE 1328

// UDP | RTP | UNIX?
namespace gurum {

class MulticastServer : public SimpleServer {
public:
	MulticastServer();
	virtual ~MulticastServer();

	void setIP(std::string ip) {
		_ip = ip;
	}
	void setPort(uint16_t port) {
		_port = port;
	}
	void setTTL(uint8_t ttl) {
		_ttl = ttl;
	}
	void setLoop(uint8_t loop) {
		_loop = loop;
	}
	void setAlias(std::string alias) {
		_alias = alias;
	}

	int send(uint8_t *buf, size_t len) OVERRIDE;
	void run(int clnt) OVERRIDE;

private:
	int open() OVERRIDE;
	struct sockaddr *sockaddr() OVERRIDE;
	size_t sockaddr_len() OVERRIDE;

private:
	uint8_t _ttl;
	uint8_t _loop;
	std::string _ip;
	std::string _alias;
	uint16_t _port;

	struct sockaddr_in _serveraddr;
	struct in_addr _iaddr;
	struct sockaddr_in _clntaddr;
};

} /* namespace gurum */

#endif /* MULTICASTSERVER_H_ */
