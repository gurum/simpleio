/*
 * NetUt.h
 *
 *  Created on: May 1, 2016
 *      Author: buttonfly
 */

#ifndef NETUT_H_
#define NETUT_H_

#include <stdint.h>
#include <sys/socket.h>
#include <string>

namespace gurum {

class NetUt {
public:
	NetUt();
	virtual ~NetUt();

	static uint32_t IP(const char* alias);
	static uint32_t defaultIP();
	static std::string dottedIP(const char *alias);
#if __linux__
	static int hwaddr(uint8_t addr[6]);
#endif
	static bool isMulticast(const struct sockaddr_storage *saddr, socklen_t len);
	static uint32_t peeraddr(int clntsck);
	static uint32_t sockaddr(int sck);
	static bool existNetworkAlias(const char *alias);
};

} /* namespace gurum */

#endif /* NETUT_H_ */
