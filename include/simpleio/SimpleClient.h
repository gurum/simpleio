/*
 * SimpleClient.h
 *
 *  Created on: Apr 13, 2016
 *      Author: buttonfly
 */

#ifndef GURUM_SIMPLECLIENT_H_
#define GURUM_SIMPLECLIENT_H_

#define USE_CC_THREAD 1

#ifndef GCC_VERSION
#define GCC_VERSION (__GNUC__ * 10000 \
				 + __GNUC_MINOR__ * 100 \
                     + __GNUC_PATCHLEVEL__)
#endif

#if GCC_VERSION > 40800
#define USE_CC_THREAD	1
#endif

#if USE_CC_THREAD
#include <thread>
#else
#include <pthread.h>
#endif
#include <stdint.h>
#include <string>

using namespace std;

namespace gurum {

class SimpleClientDelegate {
public:
	virtual void connected(){}
	virtual void disconnected(){}
	virtual bool hook(int sck, int len){return false;}
	virtual void read(uint8_t *buf, int len){}
};

class SimpleClient {
public:
	SimpleClient();
	virtual ~SimpleClient();

	void setDelegate(SimpleClientDelegate *delegate) {
		_delegate = delegate;
	}

	int start();
	void stop();

	int send(void *buf, int len);
	int send(string &msg);
	int send(const char *msg);
	void setTimeout(unsigned long ms) {
		_timeout = ms;
	}

protected:
	virtual int open()=0;
	virtual int recv(int fd, uint8_t *buf, size_t len);

protected:
	virtual void run();
#if USE_CC_THREAD
#else
	static void thr(void *arg);
#endif

protected:
	bool _stopped;
#if USE_CC_THREAD
	thread _thread;
#else
	pthread_t _thread;
#endif
	SimpleClientDelegate *_delegate;

	int _sck;
	unsigned long _timeout;
};

class SimpleTcpClient : public SimpleClient {
public:
	SimpleTcpClient();
	virtual ~SimpleTcpClient();
	void setPort(uint16_t port) {
		_port = port;
	}

protected:
	virtual int open();

protected:
	uint16_t _port;
};

class SimpleUdpClient : public SimpleTcpClient {
public:
	SimpleUdpClient();
	virtual ~SimpleUdpClient();

protected:
	virtual int open();
};

class SimpleUnixClient : public SimpleClient {
public:
	SimpleUnixClient();
	virtual ~SimpleUnixClient();

	void setUnixSockPath(const string unixSockPath) {
		_unixSockPath = unixSockPath;
	}

protected:
	virtual int open();

private:
	string _unixSockPath;
//	struct sockaddr_un _serveraddr;
};

class SimplePipeClient : public SimpleClient {
public:
	SimplePipeClient();
	virtual ~SimplePipeClient();

	void setPipePath(const string path) {
		_path = path;
	}

protected:
	virtual int open();

private:
	string _path;
//	int _fd = 0; // default: stdin
};

} /* namespace gurum */

#endif /* APPS_SIMPLEIO_SIMPLECLIENT_H_ */
