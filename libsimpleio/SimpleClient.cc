/*
 * SimpleClient.cc
 *
 *  Created on: Apr 13, 2016
 *      Author: buttonfly
 */

#include "SimpleClient.h"

#include <sys/un.h>
#include <sys/socket.h>
#include <assert.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/ioctl.h>

#define m_(...)
#define S_

namespace gurum {

SimpleClient::SimpleClient()
: _stopped(false), _delegate(NULL), _sck(-1), _timeout(0)
{
}

SimpleClient::~SimpleClient() {
	if(! _stopped) stop();
}

int SimpleClient::start() {
	_sck = open();
	assert(_sck >= 0);

#if USE_CC_THREAD
	_thread = std::thread([&](){
		for(; ! _stopped ;) run();
	});
#else
    pthread_create(&_thread, NULL, (void* (*)(void*)) thr, (void*)this);
#endif
	return 0;
}

void SimpleClient::stop() {
	_stopped = true;
#if USE_CC_THREAD
	_thread.join();
#else
	pthread_join(_thread, NULL);
#endif
	if(_sck > 0)
		close(_sck);
	_sck = -1;
}

void SimpleClient::run() {

	int ret = -1;
	int rc = -1;
	fd_set readfds;
	struct timeval tv = { 0 };
	struct timeval *p_tv = NULL;

	assert(_sck >= 0);
	FD_ZERO(&readfds);
	FD_SET(_sck, &readfds);

	if(_timeout > 0) {
		tv.tv_sec = _timeout / 1000;
		tv.tv_usec = (_timeout % 1000) * 1000;
		p_tv = &tv;
	}

	int stat = select(_sck+1, &readfds, NULL, NULL, p_tv);
	if(stat == 0) { // time-over
	}
	else if (0 < stat) {
		int n;
		if(ioctl(_sck, FIONREAD, &n) < 0)
			return;

		if(n==0) {
			FD_CLR(_sck, &readfds);
			if(_delegate) _delegate->disconnected();
		}
		else if(n > 0) {
			if(_delegate) {
				// if returning true,
				// It means that a user will read for himself.
				if(_delegate->hook(_sck, n)) return;

				uint8_t *buf = (uint8_t *) malloc(n + 1);
				memset(buf, 0, n + 1);
				assert(buf);
				n = this->recv(_sck, buf, n);
				if(_delegate) _delegate->read((uint8_t *)buf, n);

				if(buf) free(buf);
			}
		}
	}
	/* Else it timed out */
	//    printf("\nExiting : %s\n", __func__);
}

#if USE_CC_THREAD
#else
void SimpleClient::thr(void *arg) {
	SimpleClient *self = (SimpleClient *) arg;
	for(; ! self->_stopped ;) self->run();
}
#endif

int SimpleClient::send(void *buf, int len) {
	return write(_sck, buf, len);
}

int SimpleClient::send(string &msg) {
	const char *tmp = msg.c_str();
	return send((void *) tmp, (int) msg.size());
}

int SimpleClient::send(const char *msg) {
	return send((void *) msg, strlen(msg));
}

int SimpleClient::recv(int fd, uint8_t *buf, size_t len) {
	return read(fd, buf, len);
}
} /* namespace gurum */
