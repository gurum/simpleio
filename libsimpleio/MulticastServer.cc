/*
 * MulticastServer.cc
 *
 *  Created on: May 1, 2016
 *      Author: buttonfly
 */

#include "MulticastServer.h"


#define UDP_HDR_SIZE 8
#define RTP_HDR_SIZE 12

#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <string.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <getopt.h>
#include <fcntl.h>
//#include <linux/if_ether.h>
#include <net/if_arp.h>
//#include <netpacket/packet.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <net/if.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <net/if.h>
#include "NetUt.h"
#include <assert.h>

namespace gurum {

MulticastServer::MulticastServer()
: _ttl(3), _loop(1), _port(0)
{
}

MulticastServer::~MulticastServer() {
	// TODO Auto-generated destructor stub
}

int MulticastServer::open() {

	if(_port == 0) {
		fprintf(stderr, "port is not set.\n");
		return -1;
	}

	if(_ip.empty()) {
		fprintf(stderr, "ip is not set.\n");
		return -1;
	}

	fprintf(stderr, "%s:%d\n", _ip.c_str(), _port);

	int fd = socket(AF_INET, SOCK_DGRAM, 0);
	assert(fd > 0);

	memset(&_serveraddr, 0, sizeof(_serveraddr));
	const char *ip = _ip.c_str(); // guard:  is multicast?
	_serveraddr.sin_family = PF_INET;
	_serveraddr.sin_port = htons(_port); // Use the first free port
	_serveraddr.sin_addr.s_addr = inet_addr(ip); // bind socket to any interface

	if(! NetUt::isMulticast((const sockaddr_storage *) &_serveraddr, sizeof(_serveraddr))) {
		fprintf(stderr, "%s is invalid\n", _ip.c_str());
		goto exception;
	}

	if(::bind(fd, (struct sockaddr *)&_serveraddr, sizeof(struct sockaddr_in)) < 0) {
		// TODO
		fprintf(stderr, "failed to bind\n");
		goto exception;
	}

	if(_alias.empty()) {
		_iaddr.s_addr = NetUt::defaultIP(); // @deprecated
	}
	else {
		_iaddr.s_addr = NetUt::IP(_alias.c_str());
	}

	fprintf(stderr, "ip: %x\n", _iaddr.s_addr);
	setsockopt(fd, IPPROTO_IP, IP_MULTICAST_IF, &_iaddr, sizeof(struct in_addr));
	setsockopt(fd, IPPROTO_IP, IP_MULTICAST_TTL, &_ttl, sizeof(unsigned char));
	setsockopt(fd, IPPROTO_IP, IP_MULTICAST_LOOP,&_loop, sizeof(unsigned char));
	return fd;

exception:
	close(fd);
	return -1;
}

void MulticastServer::run(int clnt) OVERRIDE {
	// TODO:
	fprintf(stderr, "*");
	sleep(1);
}

int MulticastServer::send(uint8_t *buf, size_t len) {
	int addrlen = sockaddr_len();
	return sendto(_sck, buf, len, 0,(struct sockaddr *)&_serveraddr, addrlen);
}

struct sockaddr *MulticastServer::sockaddr() {
	return (struct sockaddr *) &_clntaddr;
}

size_t MulticastServer::sockaddr_len() {
	return sizeof(struct sockaddr_in);
}

} /* namespace gurum */
