/*
 * SimpleUnixClient.cc
 *
 *  Created on: May 1, 2016
 *      Author: buttonfly
 */

#include "SimpleClient.h"
#include <sys/un.h>
#include <sys/socket.h>
#include <assert.h>
#include <stdio.h>
#include <unistd.h>

namespace gurum {

SimpleUnixClient::SimpleUnixClient() {
}

SimpleUnixClient::~SimpleUnixClient() {
}

int SimpleUnixClient::open() {
	int 	fd;
	struct sockaddr_un serveraddr;
	fd = socket(PF_UNIX, SOCK_STREAM, 0);
    bzero(&serveraddr, sizeof(serveraddr));
    serveraddr.sun_family = AF_UNIX;
    strcpy(serveraddr.sun_path, _unixSockPath.c_str());

    int ret = connect(fd, (struct sockaddr *) &serveraddr, sizeof(serveraddr));
	if(ret == -1) {
		fprintf(stderr, "E: cannot connect to %s", _unixSockPath.c_str());
		return -1;
	}

	if(_delegate) _delegate->connected();
	return fd;
}

} /* namespace gurum */
