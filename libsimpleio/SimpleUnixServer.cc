/*
 * SimpleUnixServer.cc
 *
 *  Created on: Apr 13, 2016
 *      Author: buttonfly
 */

#include <unistd.h>
#include "SimpleServer.h"
#include <stdio.h>

using namespace std;

namespace gurum {

SimpleUnixServer::SimpleUnixServer() {
}

SimpleUnixServer::~SimpleUnixServer() {
}

int SimpleUnixServer::open() OVERRIDE{
	if(_unixSockPath.empty()) {
		fprintf(stderr, "You need to call setUnixSockPath()\n");
		return -1;
	}

	 int fd = socket(PF_UNIX, SOCK_STREAM, 0);
	if (fd  < 0) {
		fprintf(stderr, "failed to create a socket descriptor\n");
		return -1;
	}

	memset(&_serveraddr, 0, sizeof(_serveraddr));
	_serveraddr.sun_family = AF_UNIX;
	strcpy(_serveraddr.sun_path, _unixSockPath.c_str());

	if(::bind(fd, (struct sockaddr*) &_serveraddr, sizeof(_serveraddr)) < 0) {
		fprintf(stderr, "failed to bind\n");
	}

	if(::listen(fd, 5) < 0) {
		fprintf(stderr, "failed to listen\n");
	}
	return fd;
}

void SimpleUnixServer::stop() {
	SimpleServer::stop();
	if(! _unixSockPath.empty()) {
		::unlink(_unixSockPath.c_str());
		_unixSockPath = "";
	}
}

struct sockaddr *SimpleUnixServer::sockaddr() OVERRIDE {
	return (struct sockaddr *) &_clntaddr;
}

size_t SimpleUnixServer::sockaddr_len() OVERRIDE {
	return sizeof(struct sockaddr_un);
}

} /* namespace gurum */
